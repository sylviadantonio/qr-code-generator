<?php
/**
 * Class QRImageWithText
 *
 * example for additional text
 * @link https://github.com/chillerlan/php-qrcode/issues/35
 *
 * @filesource   QRImageWithText.php
 * @created      22.06.2019
 * @package      chillerlan\QRCodeExamples
 * @author       smiley <smiley@chillerlan.net>
 * @copyright    2019 smiley
 * @license      MIT
 */

namespace qrCodeGenerator;
use chillerlan\QRCode\Output\QRImage;

require_once("../vendor/autoload.php");
class QRCodeGenerator extends QRImage{

	/**
	 * @param string|null $file
	 * @param string|null $text
	 *
	 * @return string
	 */
	public function dump(string $file = null, string $text = null):string{
		$file ??= $this->options->cachefile;
		$fileAddress = "/qr_upload/" . $text.'.png';

		$this->image = \imagecreatetruecolor($this->length, $this->length);
		$background  = \imagecolorallocate($this->image, ...$this->options->imageTransparencyBG);
		if( ( bool )$this -> options -> imageTransparent &&
				\in_array( $this->options->outputType, $this::TRANSPARENCY_TYPES, true ) ){
				\imagecolortransparent( $this -> image, $background );
		}

		\imagefilledrectangle($this->image, 0, 0, $this->length, $this->length, $background);

		foreach($this->matrix->matrix() as $y => $row){
			foreach($row as $x => $M_TYPE){
				$this->setPixel($x, $y, $this->moduleValues[$M_TYPE]);
			}
		}

		// render text output if a string is given
		// if($text !== null){
		// 	$this->addText($text);
		// }
		$imageData = $this->dumpImage();

		if($file !== null){
			$this->saveToFile($imageData, $file);
		} else {
			try {
				file_put_contents($fileAddress, $imageData);
			} catch( Exception $e) {
				sprintf('There was an error on writting the file.' . $e);
			}
		}

		// if((bool)$this->options->imageBase64){
		// 	//file_put_contents($fileAddress, $data);
		// 	//$imageData = 'data:image/'.$this->options->outputType.';base64,'.\base64_encode($imageData);
		// }

		return $fileAddress;
	}
}
