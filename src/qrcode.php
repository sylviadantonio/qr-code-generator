<?php
namespace qrCodeGenerator;
require_once("../vendor/autoload.php");

use chillerlan\QRCode\QROptions;
use chillerlan\QRCode\QRCode;
use qrCodeGenerator\QRCodeGenerator;

if(isset($_GET['code'])) {
  return getQRCode($_GET['code']);
}

function getQRCode( $text ) {
  $PUBLIC_FOLDER_PATH = "/var/www/html/qr-code-generator/";
  $FOLDER_QR = "qr_upload/";

  $options = new QROptions([
    'version'      => 1,
    'outputType'   => QRCode::OUTPUT_IMAGE_PNG,
    'scale'        => 3,
    'imageBase64'  => false,
    'imageTransparent' => false,
    'imageTransparencyBG' => [255,255,255],
  ]);

  $fileAddress = $FOLDER_QR . $text.'.png';

  $qrOutputInterface = new QRCodeGenerator(
      $options, ( new QRCode($options) ) -> getMatrix( $text ) );

  // dump the output, with additional text
  $data = $qrOutputInterface -> dump( $PUBLIC_FOLDER_PATH . $fileAddress, $text ) ;

  echo (json_encode($FOLDER_QR . $text . ".png"));
}

?>
