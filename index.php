

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>QR-Code Generator</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style media="screen">
    .qr-img{
      width: 87px;
    }
    .qr-code-img {
      margin-top: 30px;
    }
    .qr-code-internal{
      position: relative;
      margin:auto;
      font-size: 12px;
      text-align: center;
    }
    .qr-code-internal p {
      margin-top: -10px;
    }
    .qr-code-external{
      width: 125px;
      height: 103px;
      border: dashed thin gray;
    }

    .code-name{
      position: absolute;
        bottom: -20px;
        left: 10px;
        font-weight: bolder;
        color: black;
    }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="jumbotron" style="text-align:center;">
        <h1>KG Props - QR-Code generator</h1>
      </div>
      <div class="input-group">
        <input type="text"class="form-control rent_code" name="code" id="rent_code" placeholder="Rent Code">
          <span class="input-group-btn">
            <button class="btn btn-success btn-rent-qr" type="button">Generate</button>
          </span>
      </div>
      <div class="qr-code-img">
        <div class="qr-code-external">
          <div class="qr-code-internal">
          </div>
        </div>
      </div>
    </div>
  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    $('.qr-code-external').hide()
    $("input").keypress(function (e) {
       if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
         addQR($('#rent_code').val(), 'rent_code')
       }
     });
  })


    $("body").on("click", ".btn-rent-qr", function(){
      $('.qr-code-img').html()
      let code = $('#rent_code').val()
      let img = addQR(code, 'rent_code')
    })

    const addQR = function ( code, inputName ) {
      $('#rent_code').val("")
      $('.qr-code-internal').html("")
      $('.qr-code-external').hide()
        $.ajax({
          type: "GET",
          url: 'src/qrcode.php',
          dataType: 'json',
          data:{'code': code},
          success: function (address){
            $('.qr-code-external').show()
            $('.qr-code-internal').append(`
              <img src="${address}" class="qr-img"> <p>${code}</p>
              `)
          },
          error: "alert(Oops, there was an error on processing your request.)"
        });
        $('#fulldetailsmodal').modal('show');

    }
  </script>
</html>
